#include<stdio.h>
#include<math.h>
#include<malloc.h>
#include<stdlib.h>
#include<string.h>
#include<sys/time.h>
#include <postgresql/libpq-fe.h>
#include<dirent.h>

#define r_a 0.000277778
#define r_b 0.000277778

#define PI 3.1415926535

typedef struct QCstar
{
   double ra;
   double dec;
   int  healpixid;
   int  htmid;
   int starid;
   float mag;
}QCstar;

//Get the number of records which have the same HTM or HEALPix
int query(int , int);

//Get the number of files
void readFileList(char *);

//Create reference table
void create_table();

//Insert reference table
void insert_table(char *, char *, char *, char *, int , char *);

char file_name[500][20];

int file_num;

int line;

PGconn *g_conn;
PGresult *result;

const char *memorytable = "querytable";

char sql[2000];

static QCstar match[100];
double min_dis;
char ra_s[50];
char dec_s[50];
char healpixid_s[50];
char htmid_s[50];
char starid_s[50];
char mag_s[50];
char magerr_s[50];

int main(int argc, char **argv)
{
	int i, j, m, k;
	FILE *fp;
	
	struct timeval beg, end;
	gettimeofday(&beg, 0);

	if(argc != 2)
	{
		printf("Please input the TXT files\n");
		printf("Usage: %s [filename]\n", argv[0]);
		exit(1);
	}

	min_dis = r_a * r_a + r_b * r_b;

	g_conn = NULL;
	
	g_conn = PQconnectdb("user=postgres password=1 dbname=mydb hostaddr=127.0.0.1 port=5432");

	if(PQstatus(g_conn) == CONNECTION_BAD)
	{
		printf("sorry, no database connect...\n");
		PQfinish(g_conn);
		return 1;
	}

	create_table();

	line = 0;
	
	double ra, dec;
	float mag, magerr;
	int htmid, healpixid;
	char st1[15], st2[15], outf[50];
	FILE *out;
	
	readFileList(argv[1]);

	char df[50];

	for(i = 0; i < file_num; i++)
	{
		sprintf(df, "%s", argv[1]);
		strcat(df, "/");
		strcat(df, file_name[i]);

		fp = fopen(df, "r");

		if(fp == NULL)
			exit(1);
		
		char name[50]="";
		
		k = 0;

		while(file_name[i][k] != '.')
		{
			name[k] = file_name[i][k];
			k++;
		}
		name[k] = '\0';
		
		sprintf(outf, "/home/lk/cmp/out/out_");
		strcat(outf, name);
		strcat(outf,".csv");
		out = fopen(outf, "a");
		
		while(~fscanf(fp, "%lf %lf %d %d %s %s %f %f", &ra, &dec, &healpixid, &htmid, st1, st2, &mag, &magerr))
		{
			m = query(healpixid, htmid);
		
			sprintf(ra_s, "%lf", ra);
			sprintf(dec_s, "%lf", dec);
			sprintf(healpixid_s, "%d", healpixid);
			sprintf(htmid_s,"%d", htmid); 
			sprintf(mag_s, "%f", mag);

			int flag = 0;
			int starid;
			
			if(m == 0)
			{
				line++;
				insert_table(ra_s, dec_s, healpixid_s, htmid_s, line, mag_s);
				flag = 1;
				fprintf(out, "%lf, %lf, %d, %d, %s %s, %f, %f, %d\n", ra, dec, healpixid, htmid, st1, st2, mag, magerr, line);
			}
			else
			{
				double dis;
				for(j = 0; j < m; j++)
				{
					//Location-based cross-matching
					dis = pow((ra - match[j].ra) * ((double)cos((dec + match[j].dec) / 2.0 / 180.0 * PI)), 2.0)+pow((dec - match[j].dec), 2.0);
					if(dis < min_dis)
					{
						flag = 1;
						starid = match[j].starid;
						fprintf(out, "%lf, %lf, %d, %d, %s %s, %f, %f, %d\n", ra, dec, healpixid, htmid, st1, st2, mag, magerr, starid);
					}
				}
			}
			
			if(flag == 0)
			{
				line++;
				insert_table(ra_s, dec_s, healpixid_s, htmid_s, line, mag_s);
				fprintf(out, "%lf, %lf, %d, %d, %s %s, %f, %f, %d\n", ra, dec, healpixid, htmid, st1, st2, mag, magerr, line);
			}
		}
		fclose(fp);
		fclose(out);
	}
	
	PQfinish(g_conn);
	PQclear(result);

	gettimeofday(&end, 0);
	double resadd_time = (end.tv_sec - beg.tv_sec) * 1000 + (double)(end.tv_usec - beg.tv_usec) / 1000.0;
	printf("total time use %lf msec\n",resadd_time);

	return 0;	
}

void create_table()
{
	char sql[2000];
	
	sprintf(sql, "CREATE unlogged TABLE IF NOT EXISTS %s(id  SERIAL PRIMARY KEY, ra double precision NOT NULL, dec double precision NOT NULL, healpixId bigint NOT NULL, htmId bigint NOT NULL, matchId bigint NOT NULL, mag real NOT NULL);", memorytable);

	result = PQexec(g_conn, sql);

	if (PQresultStatus(result) != PGRES_COMMAND_OK)
	{
		printf("%s\n%s\n", sql, PQerrorMessage(g_conn));
		printf("Create reference table fail\n");
		PQclear(result);
		return;
	}
	
	sprintf(sql, "CREATE INDEX IF NOT EXISTS index ON %s (healpixId);", memorytable);
	
	result = PQexec(g_conn, sql);
	
	if (PQresultStatus(result) != PGRES_COMMAND_OK)
	{
		printf("%s\n%s\n", sql, PQerrorMessage(g_conn));
		printf("Create index fail\n");
		PQclear(result);
		return;
	}

	sprintf(sql, "CREATE INDEX IF NOT EXISTS index_htm ON %s (htmId);", memorytable);
	
	result = PQexec(g_conn, sql);
	
	if (PQresultStatus(result) != PGRES_COMMAND_OK)
	{
		printf("%s\n%s\n", sql, PQerrorMessage(g_conn));
		printf("Create index fail\n");
		PQclear(result);
		return;
	}
}

//Get the name of sky zoning file
void readFileList(char *basePath)
{
    DIR *dir;

    struct dirent *ptr;

    file_num = 0;

    if ((dir = opendir(basePath)) == NULL)
    {
        perror("Open dir error...\n");
        exit(1);
    }

    while ((ptr=readdir(dir)) != NULL)
    {
    	if(strcmp(ptr -> d_name,".") == 0 || strcmp(ptr -> d_name,"..")==0)    //current dir OR parrent dir
    		continue;
    	sprintf(file_name[file_num], "%s", ptr -> d_name);
    	file_num++;
    }
    closedir(dir);
}

int query(int healpixid, int htmid)
{
	char query[2000]="";
	int star_num=0;
	int i, j;

	char healpixid_s[50], htmid_s[50];
	sprintf(healpixid_s, "%d", healpixid);
	sprintf(htmid_s, "%d", htmid);

	strcpy(query, "SELECT * FROM ");
	strcat(query, memorytable);
	strcat(query, " WHERE healpixId = "); 
	strcat(query, healpixid_s);
	strcat(query, " OR htmId = "); 
	strcat(query, htmid_s);

	result = PQexec(g_conn, query);

	if(PQresultStatus(result) != PGRES_TUPLES_OK) 
	{
		printf("%s\n%s\n", query, PQerrorMessage(g_conn));
		printf("Exec Query Fauled!\n");
		PQclear(result);	   
		return 0;   
	}
     
	i = PQntuples(result);
   
	for(j = 0; j < i; j++)
	{       
		match[star_num].ra = atof(PQgetvalue(result, j, 1));
		match[star_num].dec = atof(PQgetvalue(result, j, 2));
		match[star_num].healpixid = atoi(PQgetvalue(result, j, 3));
		match[star_num].htmid = atoi(PQgetvalue(result, j, 4));
		match[star_num].starid = atoi(PQgetvalue(result, j, 5));
		match[star_num].mag = atof(PQgetvalue(result, j, 6));
		star_num++;
	}
   
	PQclear(result);

	return star_num;
}

void insert_table(char *ra_s, char *dec_s, char *healpixid_s, char *htmid_s, int line, char *mag_s)
{

	char sql[2000]="";

	sprintf(starid_s, "%d", line);

	strcpy(sql, "INSERT INTO ");
	strcat(sql, memorytable);
	strcat(sql, " (ra, dec, healpixId, htmId, matchId, mag) VALUES ( ");
	strcat(sql, ra_s);
	strcat(sql," , ");
	strcat(sql, dec_s);
	strcat(sql," , ");
	strcat(sql, healpixid_s);
	strcat(sql," , ");
	strcat(sql, htmid_s);
	strcat(sql," , ");
	strcat(sql, starid_s);
	strcat(sql," , ");
	strcat(sql, mag_s);
	strcat(sql," ); ");
	
	result = PQexec(g_conn, sql);	

	if (PQresultStatus(result) != PGRES_COMMAND_OK)
	{
		printf("%s\n%s\n", sql, PQerrorMessage(g_conn));
		printf("INSERT mem Q ERROR!!!\n");
		return;
	}
}
