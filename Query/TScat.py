#!/usr/bin/python

import os
import sys
import math
import datetime


#Get the name of file
def file_name(file_dir):
	L = []
	for root, dirs, files in os.walk(file_dir):
		for file in files:
			if os.path.splitext(file)[1] == ".txt":
				L.append(os.path.join(root,file))
	return L

if __name__ == '__main__':
	if len(sys.argv) != 3:
		print("Please input the command correctly: ")
		print("python TScat.py input output")
		exit(1)
	
	
	#starttime = datetime.datetime.now()

	inputpath = sys.argv[1]
	outputpath = sys.argv[2]
	
	#Traverse the files in the folder to transfer
	l = file_name(inputpath)
	
	for tar in l:
		tarf = open(tar)
		tarline = tarf.readline()[:-1]
		tar_dict = {}
		while tarline :
			data = tarline.split(",")
			tar_list = []
			tar_list = tar_dict.get(int(data[6]),tar_list)
			tar_list.append(data)
			tar_dict[int(data[6])] = tar_list
			tarline = tarf.readline()[:-1]
		tarf.close()
		
		f = tar.split('/')[1]
		f = f.split('.')[0]
		output = outputpath + "/" + f
		isExists = os.path.exists(output)

		if not isExists:
			os.makedirs(output) 

		for i in tar_dict:
			if len(tar_dict[i]) >= 5:
				out_path = output + "/" + str(i) + ".csv"
				out_file = open(out_path, "a+")
				out_list = tar_dict.get(i)
				for j in out_list:
					out_file.write(",".join(j) + "\n")

	

	#endtime = datetime.datetime.now()
	#print (endtime - starttime).seconds
	

